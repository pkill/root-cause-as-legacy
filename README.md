# Root cause as legacy

Root cause is imprecise. Here's my attempt to establish more precise terminology to replace each instance of "root cause".

| Root cause as… | Use this instead |
| ------ | ------ |
| The ‘lit match’ or ‘first domino.' “_I think the most recent deploy was the root cause._”| Trigger “_I think the most recent deploy was the trigger._”|
| The explanation or hypothesis that could lead to a mitigation. “_Have we found the root cause yet?_” | Diagnosis “_Have we made a diagnosis yet?_”|
| The act of forming a hypothesis around instability. “_I’m in the middle of root causing this issue._” | Diagnose “_I’m in the middle of diagnosing this issue._”|
| The actions and changes that put things on the path to stabilization. “_It turned out that upgrading to a newer version addressed the issue so that was the root cause._” | Mitigation “_It turned out that upgrading to a newer version addressed the issue so that was the mitigation._” |
| The set of multiple causes each necessary but not sufficient. “_I’m attempting to enumerate the root causes._” | Contributing factors “_I’m attempting to enumerate the contributing factors._”|
| A mechanistic accounting of the various contributors. “_Tomorrow we’ll deliver our root cause analysis (RCA) to the customer._” | Causal analysis “_Tomorrow we’ll deliver our causal analysis to the customer._” |
| The technical description of how the components interacted leading to the emergent behavior. “_It became clear why users were stuck after my team finished researching the root cause._” | Mechanism of failure “_It became clear why users were stuck after my team finished researching the mechanism of failure._” |
